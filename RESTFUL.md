
# API RESTFUL

Une version un peu simplifiée mais quand même pas mal des specification d'API (pas hésiter à chercher sur internet des trucs [dans ce genre là](https://wiki.onap.org/display/DW/RESTful+API+Design+Specification) pour plus de détails)

- [API RESTFUL](#api-restful)
  * [Les différentes routes et ce qu'elles renvoient](#les-diff-rentes-routes-et-ce-qu-elles-renvoient)
    + [GET /api/person](#get--api-person)
      - [Exemple de body](#exemple-de-body)
      - [Exemple de retour](#exemple-de-retour)
      - [Paramètres optionnels](#param-tres-optionnels)
    + [GET /api/person/1](#get--api-person-1)
      - [Exemple de body](#exemple-de-body-1)
      - [Exemple de retour](#exemple-de-retour-1)
    + [POST /api/person](#post--api-person)
      - [Exemple de body](#exemple-de-body-2)
      - [Exemple de retour](#exemple-de-retour-2)
    + [PATCH /api/person/1](#patch--api-person-1)
      - [Exemple de body](#exemple-de-body-3)
      - [Exemple de retour](#exemple-de-retour-3)
    + [DELETE /api/person/1](#delete--api-person-1)
      - [Exemple de body](#exemple-de-body-4)
      - [Exemple de retour](#exemple-de-retour-4)


## Les différentes routes et ce qu'elles renvoient
Voilà les différentes routes pour un CRUD basique, on va prendre l'exemple d'un objet person avec un id, un name et un age.


### GET /api/person
* Renvoie un status 200 si succès.
#### Exemple de body
Pas de body
#### Exemple de retour
```json
[
    {
        "id":1,
        "name":"Test 1",
        "age":25
    },
    {
        "id":2,
        "name":"Test 2",
        "age":30
    }
]
```


#### Paramètres optionnels
On pourra rajouter des paramètres optionnels de tri, de pagination, de filtre sous cette forme : `/api/person?age=20&limit=10` (récupèrer les personnes qui ont 20 ans et n'en récupérer que 10)

### GET /api/person/1
* Renvoie un status 200 si la personne avec l'id 1 est trouvée en bdd
* Si la personne n'est pas trouvée, renvoie un 404
#### Exemple de body
Pas de body
#### Exemple de retour
```json
{
    "id":1,
    "name":"Test 1",
    "age":25
}
```

### POST /api/person
* Renvoie un status 201 (created) si l'ajout se passe bien
* Si erreur de validation des paramètres envoyés, renvoie un 400 (bad request) 
#### Exemple de body
La personne a faire persister
```json
{
    "name":"Test 1",
    "age":25
}
```

#### Exemple de retour
* Si succès, renvoie la personne complète post persistence (donc la personne avec son id)
```json
{
    "id":10,
    "name":"Test 1",
    "age":25
}
```
* Si erreur, peut renvoyer les messages d'erreurs
```json
{
    "errors" : {
        "name":"Name should not be empty",
        "age": "Age should not be negative"
    }
}
```

### PATCH /api/person/1
* Si succès renvoie un 200
* Si la personne 1 n'est pas trouvée renvoie un 404 (Not Found)
* Si erreur de validation des paramètres envoyés, renvoie un 400 (bad request)
#### Exemple de body
La personne a faire persister
```json
{
    "name":"Test Modifié"
}
```
#### Exemple de retour
* Si succès, renvoie la personne complète post modification
```json
{
    "id":1,
    "name":"Test Modifié",
    "age":25
}
```
* Si erreur, peut renvoyer les messages d'erreurs
```json
{
    "errors" : {
        "name":"Name should not be empty"
    }
}
```

### DELETE /api/person/1
* Si succès renvoie un 204 (Success - No Content)
* Si la personne 1 n'est pas trouvée renvoie un 404 (Not Found)

#### Exemple de body
Pas de body

#### Exemple de retour
Pas de retour
